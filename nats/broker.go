/// Copyright (c) OpenFaaS Author(s) 2020. All rights reserved.
/// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package nats

import (
	"fmt"
	"log"
	"time"

	"github.com/adshao/go-binance/v2"
	nats "github.com/nats-io/nats.go"
)

const clientName = "binance_connector"

// BrokerConfig high level config for the broker
type BrokerConfig struct {

	// Host is the NATS address, the port is hard-coded to 4222
	Host string

	// ConnTimeout is the timeout for Dial on a connection.
	ConnTimeout time.Duration
}

// Broker used to subscribe to NATS subjects
type Broker interface {
	SubscribeTrade(binance.WsTradeHandler, []string) ([]chan struct{}, error)
	SubscribeAggTrade(binance.WsAggTradeHandler, []string) ([]chan struct{}, error)
	SubscribeKline(binance.WsKlineHandler, string, []string) ([]chan struct{}, error)
	SubscribeDepth(binance.WsDepthHandler, []string) ([]chan struct{}, error)
	Publish(channel string, data string) error
	Close()
}

type broker struct {
	client *nats.Conn
}

// NATSPort hard-coded port for NATS
const NATSPort = "4222"

// NewBroker loops until we are able to connect to the NATS server
func NewBroker(config BrokerConfig) (Broker, error) {
	broker := &broker{}
	brokerURL := fmt.Sprintf("nats://%s:%s", config.Host, NATSPort)

	for {
		client, err := nats.Connect(brokerURL,
			nats.Timeout(config.ConnTimeout),
			nats.Name(clientName))

		if client != nil && err == nil {
			broker.client = client
			break
		}

		if client != nil {
			client.Close()
		}

		log.Println("Wait for brokers to come up.. ", brokerURL)
		time.Sleep(1 * time.Second)
		// TODO Add healthcheck
	}

	return broker, nil
}

func (b *broker) SubscribeTrade(handler binance.WsTradeHandler, symbols []string) ([]chan struct{}, error) {
	log.Printf("Configured Trade symbols: %v", symbols)

	if b.client == nil {
		return nil, fmt.Errorf("client was nil, try to reconnect")
	}

	errHandler := func(err error) {
		fmt.Println(err)
	}

	var subs []chan struct{}

	out := make(chan chan struct{})
	for _, symbol := range symbols {
		go func(symbol string, errHandler binance.ErrHandler) {
			log.Printf("Connect websocket Trade for symbol: %q", symbol)

			doneC, _, err := binance.WsTradeServe(symbol, handler, errHandler)
			if err != nil {
				log.Fatal(err)
			}

			if err != nil {
				log.Printf("Unable to connect ws Trade to symbol: %s", symbol)
			}

			out <- doneC
		}(symbol, errHandler)
	}

	for range symbols {
		doneC := <-out
		subs = append(subs, doneC)
	}

	return subs, nil
}

func (b *broker) SubscribeAggTrade(handler binance.WsAggTradeHandler, symbols []string) ([]chan struct{}, error) {
	log.Printf("Configured AggTrade symbols: %v", symbols)

	if b.client == nil {
		return nil, fmt.Errorf("client was nil, try to reconnect")
	}

	errHandler := func(err error) {
		fmt.Println(err)
	}

	var subs []chan struct{}

	out := make(chan chan struct{})
	for _, symbol := range symbols {
		go func(symbol string, errHandler binance.ErrHandler) {
			log.Printf("Connect websocket AggTrade for symbol: %q", symbol)

			doneC, _, err := binance.WsAggTradeServe(symbol, handler, errHandler)
			if err != nil {
				log.Fatal(err)
			}

			if err != nil {
				log.Printf("Unable to connect ws AggTrade to symbol: %s", symbol)
			}

			out <- doneC
		}(symbol, errHandler)
	}

	for range symbols {
		doneC := <-out
		subs = append(subs, doneC)
	}

	return subs, nil
}

func (b *broker) SubscribeKline(handler binance.WsKlineHandler, interval string, symbols []string) ([]chan struct{}, error) {
	log.Printf("Configured Kline  '%s' symbols: %v", interval, symbols)

	if b.client == nil {
		return nil, fmt.Errorf("client was nil, try to reconnect")
	}

	errHandler := func(err error) {
		fmt.Println(err)
	}

	var subs []chan struct{}
	out := make(chan chan struct{})
	for _, symbol := range symbols {
		go func(symbol string, errHandler binance.ErrHandler) {
			log.Printf("Connect websocket Kline '%s' for symbol: %q", interval, symbol)

			doneC, _, err := binance.WsKlineServe(symbol, interval, handler, errHandler)
			if err != nil {
				log.Fatal(err)
			}

			if err != nil {
				log.Printf("Unable to connect ws Kline '%s' to symbol: %s", interval, symbol)
			}
			out <- doneC
		}(symbol, errHandler)
	}

	for range symbols {
		doneC := <-out
		subs = append(subs, doneC)
	}

	return subs, nil
}

func (b *broker) SubscribeDepth(handler binance.WsDepthHandler, symbols []string) ([]chan struct{}, error) {
	log.Printf("Configured Depth symbols: %v", symbols)

	if b.client == nil {
		return nil, fmt.Errorf("client was nil, try to reconnect")
	}

	errHandler := func(err error) {
		fmt.Println(err)
	}

	var subs []chan struct{}

	out := make(chan chan struct{})
	for _, symbol := range symbols {
		go func(symbol string, errHandler binance.ErrHandler) {
			log.Printf("Connect websocket Depth for symbol: %q", symbol)
			doneC, _, err := binance.WsDepthServe(symbol, handler, errHandler)
			if err != nil {
				log.Fatal(err)
			}

			if err != nil {
				log.Printf("Unable to connect ws Depth to symbol: %s", symbol)
			}

			out <- doneC
		}(symbol, errHandler)
	}

	for range symbols {
		doneC := <-out
		subs = append(subs, doneC)
	}

	return subs, nil
}

func (b *broker) Publish(channel string, data string) error {
	if b.client == nil {
		return fmt.Errorf("client was nil, try to reconnect")
	}
	return b.client.Publish(channel, []byte(data)) //b.client.Publish("crypto."+channel, []byte(data))
}

func (b *broker) Close() {
	b.client.Close()
}
