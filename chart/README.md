# Binance Connector

The [Binance connector](https://gitlab.com/bullshit/binance-connector) publishes binance websocket data into nats topics.


## Install the Chart

- Add the chart repo and deploy the `binance-connector` chart. We recommend installing it in the same namespace as the rest of your nats broker

```sh
$ helm repo add #REPONAME# https://#TBD#
$ helm upgrade nats-connector #REPONAME#/nats-connector \
    --install \
    --namespace openfaas
```

## Configuration

Additional nats-connector options in `values.yaml`.

| Parameter                | Description                                                                            | Default                        |
| ------------------------ | -------------------------------------------------------------------------------------- | ------------------------------ |
| `broker_host`            | location of the nats brokers.                                                         | `nats`                        |
| `symbols`                | list of symbols separated by symbols_delimiter.         | `btcbusd,`                 |
| `symbols_delimiter`      | Delimiter for symbol names.            | `,`                 |
| `env`       | Environment to seperate channels e.g: dev-crypto-trade. "prod" results in blank topic prefix.    | `prod`                          |
| `upstream_timeout`       | Maximum timeout for upstream function call, must be a Go formatted duration string.    | `30s`                          |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

```sh
helm install \
    binance-connector binance-connector/\
    -n namespace \
    --atomic \
    --set symbols="btcbusd\,btceur\,bnbeur\,bnbbusd\,bnbbtc\,ltceur\,ltcbusd\,ltcbtc\,ethbusd\,etheur\,ethbnb\,ethbtc" \
    --set env=dev
    --set imagePullSecrets[0].name=kube-setup-k8s-dev-registry
```


# broker hostname
broker_host: "nats"
# list of symbols separated by symbols_delimiter
symbols: "btcbusd,"
# delimiter for symbol names
symbols_delimiter: ","
# environment to seperate channels e.g: dev-crypto-trade. "prod" results in blank topic prefix
env: "prod"
# timeout for upstream calls
upstream_timeout: 30s