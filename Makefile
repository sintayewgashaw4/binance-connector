TAG?=latest-dev
NAMESPACE?=bullshit
.PHONY: build

build:
	docker build -t $(NAMESPACE)/binance-connector:$(TAG) .

push:
	docker push $(NAMESPACE)/binance-connector:$(TAG)
