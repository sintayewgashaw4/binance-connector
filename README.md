# Binance connector

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The [Binance connector](https://gitlab.com/bullshit/binance-connector) publishes [binance.com](https://binance-docs.github.io/apidocs/spot/en/#websocket-market-streams) websocket data into nats topics.

## Try it out

Test functions have been created to help you verify the installation of `binance-connector`.  See [`contrib/test-functions`](./contrib/test-functions).

### Deploy on Kubernetes

The following instructions show how to run and test `binance-connector` on Kubernetes.

1. Deploy the receiver functions, the receiver function must have the `topic` annotation:

   Deploy with the `stack.yml` provided in this repo:
   ```
   cd contrib/test-functions
   faas-cli template pull stack
   faas-cli deploy --filter receive-message
   ```

2. Deploy the connector with:

   ```bash
   kubectl apply -f ./yaml/kubernetes/connector-dep.yaml
   ```

4. Verify that the receiver was invoked by checking the logs

   ```bash
   faas-cli logs receive-message

   2021/03/26 17:04:13 POST / - 200 OK - ContentLength: 210
   2021/03/26 17:04:13 stderr: 2021/03/26 17:04:13 received "{\"e\":\"aggTrade\",\"E\":1616778253100,\"s\":\"BTCBUSD\",\"a\":117248067,\"p\":\"53742.37000000\",\"q\":\"0.00675600\",\"f\":135950937,\"l\":135950937,\"T\":1616778253099,\"m\":false,\"M\":true}"
   ```

## Building

Build and release is done via CI, but you can also build your own version locally.

```bash
export TAG=0.2.1
make build push
```

### Configuration

Configuration is by environment variable, which can be set in the Kubernetes YAML file: [yaml/kubernetes/connector-dep.yaml](./yaml/kubernetes/connector-dep.yaml)

| Parameter                | Description                                                                            | Default                        |
| ------------------------ | -------------------------------------------------------------------------------------- | ------------------------------ |
| `broker_host`            | Location of the nats brokers.                                                         | `nats`                        |
| `symbols`                | List of symbols separated by symbols_delimiter.         | `btcbusd,`                 |
| `symbols_delimiter`      | Delimiter for symbol names.            | `,`                 |
| `env`       | Environment to seperate channels e.g: dev-crypto-trade. "prod" results in blank topic prefix.    | `prod`                          |
| `upstream_timeout`       | Maximum timeout for upstream function call, must be a Go formatted duration string.    | `30s`                          |


## Forked

Most of the work is based on [Nats connector](https://github.com/openfaas/nats-connector) see [LICENSE.fork](LICENSE.fork)